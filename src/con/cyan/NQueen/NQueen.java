package con.cyan.NQueen;

import java.util.ArrayList;
import java.util.Scanner;

public class NQueen {

	public int boardSize;
	public MyArrayObject rows;
	public ArrayList<MyArrayObject> total;

	public NQueen(int size) {
		boardSize = size;
		rows = new MyArrayObject(size);
		total = new ArrayList<MyArrayObject>();
	}

	public boolean checkConstraints(int[] A, int n, int r) {
		for (int i = 1; i <= r - 1; i++) {
			if ((A[i] == A[r]) || ((A[r] - (r - i)) == A[i])
					|| ((A[r] + (r - i)) == A[i])) {
				return false;
			}
		}
		return true;
	}

	public boolean ForwardCheck(int[] X, int i, int j, boolean[][] f) {
		X[i] = j;
		f[i][j] = false;
		for (int k = 1; k < boardSize; k++) {
			f[i][k] = false;
		}
		for (int l = 1; l < boardSize; l++) {
			f[l][j] = false;
		}
		for (int m = 1;; m++) {
			if (i - m == 0 || j - m == 0) {
				break;
			}
			f[i - m][j - m] = false;
		}
		for (int n = 1;; n++) {
			if (i + n == boardSize || j + n == boardSize) {
				break;
			}
			f[i + n][j + n] = false;
		}
		for (int o = 1;; o++) {
			if (i + o == boardSize || j - o == 0) {
				break;
			}
			f[i + o][j - o] = false;
		}
		for (int p = 1;; p++) {
			if (i - p == 0 || j + p == boardSize) {
				break;
			}
			f[i - p][j + p] = false;
		}
		boolean finish = true;
		for (int row = 1; row < boardSize; row++) {
			if (X[row] == 0) {
				finish = false;
			}
		}
		if (finish) {
			return true;
		}
		for (int q = 1; q < boardSize; q++) {
			for (int r = 1; r < boardSize; r++) {
				if (f[q][r] == true) {
					return true;
				}
			}
		}
		return false;
	}

	public int SelectRowMRV(boolean[][] flag) {
		int row = 0;
		int max = -1;
		for (int i = 1; i < boardSize; i++) {
			int count = 0;
			for (int j = 1; j < boardSize; j++) {
				if (flag[i][j] == false) {
					count++;
				}
			}
			if (count > max && count != boardSize - 1) {
				max = count;
				row = i;
			}
		}
		return row;
	}

	public boolean placeQueen(MyArrayObject X, int N, int i) {
		if (i == N) {
			for (int j = 1; j < boardSize; j++) {
				System.out.print(X.rows[j] + "  ");
			}
			System.out.println("\n");
			return true;
		}

		for (int j = 1; j < N; j++) {
			X.rows[i] = j;
			boolean flag = checkConstraints(X.rows, N, i);
			if (!flag) {
				continue;
			}
			boolean success = placeQueen(X, N, i + 1);
			if (success) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unused")
	public boolean placeQueens(MyArrayObject X, int N, int i) {
		if (i == N) {
			MyArrayObject temp = new MyArrayObject(N);
			for (int j = 1; j < N; j++) {
				temp.rows[j] = X.rows[j];
			}
			total.add(temp);
			return true;
		}

		for (int j = 1; j < N; j++) {
			X.rows[i] = j;
			boolean flag = checkConstraints(X.rows, N, i);
			if (!flag) {
				continue;
			}
			boolean succ =placeQueens(X, N, i + 1);
		}
		return false;
	}

	public boolean PlaceNQueenFCandMRV(MyArrayObject X, int N, boolean[][] flg) {
		boolean fill = true;
		for (int i = 1; i < boardSize; i++) {
			if (X.rows[i] == 0) {
				fill = false;
				break;
			}
		}
		if (fill) {
			MyArrayObject temp = new MyArrayObject(N);
			for (int j = 1; j < N; j++) {
				temp.rows[j] = X.rows[j];
			}
			total.add(temp);
			return true;
		}
		int i = SelectRowMRV(flg);

		if (i == 0)
			return false;
		for (int j = 1; j < boardSize; j++) {
			if (flg[i][j]) {
				X.rows[i] = j;
				boolean[][] temp = new boolean[boardSize][boardSize];
				for (int k = 1; k < boardSize; k++) {
					for (int l = 1; l < boardSize; l++) {
						temp[k][l] = flg[k][l];
					}
				}
				boolean flag = ForwardCheck(X.rows, i, j, temp);
				if (flag == false)
					continue;
				@SuppressWarnings("unused")
				boolean succ = PlaceNQueenFCandMRV(X, N, temp);

			}
		}
		X.rows[i] = 0;
		return false;
	}

	public void printArrayList() {
		System.out.println(total.size());
		for (MyArrayObject element : total) {
			element.print();
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter board size: ");
		int number = Integer.parseInt(scanner.next());
		scanner.close();
		System.out.println("Simple Backtrack:");
		NQueen queen = new NQueen(number + 1);
		queen.placeQueens(queen.rows, queen.boardSize, 1);
		queen.printArrayList();
		System.out.println("Backtrack with Forward Check & MRV");
		NQueen queen1 = new NQueen(number + 1);
		boolean[][] f = new boolean[number + 1][number + 1];
		for (int i = 1; i < number + 1; i++) {
			for (int j = 1; j < number + 1; j++) {
				f[i][j] = true;
			}
		}
		queen1.PlaceNQueenFCandMRV(queen1.rows, queen1.boardSize, f);
		queen1.printArrayList();
		// System.out.println("here"+queen.ForwardCheck(queen.rows.rows, 2, 3,
		// f));
		// System.out.println();
		// System.out.println("row: "+queen.SelectRowMRV(f));
		// for(int i=1; i<number+1;i++)
		// {
		// for(int j=1;j<number+1;j++)
		// {
		// System.out.print(f[i][j]+"   ");
		// }
		// System.out.print("\n");
		// }
		// System.out.println("");
		// System.out.println("here"+queen.ForwardCheck(queen.rows.rows, 2, 1,
		// f));
		// System.out.println("row: "+queen.SelectRowMRV(f));
		// for(int i=1; i<number+1;i++)
		// {
		// for(int j=1;j<number+1;j++)
		// {
		// System.out.print(f[i][j]+"   ");
		// }
		// System.out.print("\n");
		// }
		// System.out.println("");
		// System.out.println("here"+queen.ForwardCheck(queen.rows.rows, 1, 2,
		// f));
		// System.out.println("row: "+queen.SelectRowMRV(f));
		// for(int i=1; i<number+1;i++)
		// {
		// for(int j=1;j<number+1;j++)
		// {
		// System.out.print(f[i][j]+"   ");
		// }
		// System.out.print("\n");
		// }
	}

}

class MyArrayObject {
	public int[] rows;
	public int size;

	public MyArrayObject(int boardSize) {
		size = boardSize;
		rows = new int[boardSize];
		for (int i = 0; i < boardSize; i++) {
			rows[i] = 0;
		}
	}

	public void print() {
		for (int j = 1; j < size; j++) {
			System.out.print(rows[j] + "  ");
		}
		System.out.println("\n");
	}
}
